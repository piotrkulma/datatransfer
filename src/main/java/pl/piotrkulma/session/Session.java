package pl.piotrkulma.session;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Session {
    private String id;
    private List<String> commandAction;

    public Session(final String id) {
        this.id = id;
        this.commandAction = new ArrayList<>();
    }

    public void addAction(final String command) {
        commandAction.add(command);
    }

    public Optional<String> getLastAction() {
        if(commandAction.size() > 0) {
            return Optional.of(commandAction.get(commandAction.size() - 1));
        }

        return Optional.empty();
    }

    public String getId() {
        return id;
    }
}
