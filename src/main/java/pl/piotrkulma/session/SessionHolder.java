package pl.piotrkulma.session;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class SessionHolder {
    private List<Session> sessionList;

    public SessionHolder() {
        this.sessionList = new ArrayList<>();
    }

    public void createSession(final String id) {
        sessionList.add(new Session(id));
    }

    public void removeSession(final String id) {
        Iterator<Session> clientIterator = sessionList.iterator();

        while(clientIterator.hasNext()) {
            Session session = clientIterator.next();
            if(session.getId().equals(id)) {
                clientIterator.remove();
                break;
            }
        }
    }

    public Optional<Session> findSession(final String id) {
        return sessionList.stream()
                .filter(session -> session.getId().equals(id))
                .findFirst();
    }
}
