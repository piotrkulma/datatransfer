package pl.piotrkulma.commands;

import pl.piotrkulma.server.exceptions.ConnectionClosedError;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.Optional;

public final class DataExchangeUtils {
    private static final int ZERO = 0;
    private static final int CONNECTION_ERROR = -1;

    private static final int SEND_DATA_PACKET_SIZE = 1024;
    private static final int READ_DATA_PACKET_SIZE = 1024;
    private static final int OTHER_COMMAND_PACKET_SIZE = 1024;

    public static void sendDataFromFile(final SocketChannel clientChannel, final String fileName) {
        int bytesRead = -1;
        byte[] data = new byte[SEND_DATA_PACKET_SIZE];
        ByteBuffer buffer = ByteBuffer.allocate(SEND_DATA_PACKET_SIZE);

        try {
            FileInputStream fileInputStream = new FileInputStream(new File(fileName));

            while ((bytesRead = fileInputStream.read(data)) != -1) {
                buffer.put(data, 0, bytesRead);
                buffer.flip();
                clientChannel.write(buffer);
                buffer.clear();
            }
            fileInputStream.close();
        } catch (Exception e) {
            throw new ConnectionClosedError("Error while transferring data: " + e.getMessage());
        }
    }

    public static void saveDataToFile(final SocketChannel clientChannel, final String fileName) {
        int bytesRead;
        ByteBuffer buffer = ByteBuffer.allocate(READ_DATA_PACKET_SIZE);

        try {
            OutputStream outputStream = new FileOutputStream(new File(fileName));

            while ((bytesRead = clientChannel.read(buffer)) > 0) {
                buffer.flip();
                outputStream.write(buffer.array(), 0, bytesRead);
                buffer.clear();
            }

            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            throw new ConnectionClosedError("Error while transferring data: " + e.getMessage());
        }

        if (bytesRead == CONNECTION_ERROR) {
            throw new ConnectionClosedError("Connection closed");
        }
    }

    public static Optional<String> fetchStringData(final SocketChannel clientChannel) {
        int bytesRead;
        ByteBuffer buffer = ByteBuffer.allocate(OTHER_COMMAND_PACKET_SIZE);

        try {
            bytesRead = clientChannel.read(buffer);
        } catch (IOException e) {
            throw new RuntimeException("Error while reading command from channel: " + e.getMessage());
        }

        if (bytesRead == ZERO) {
            return Optional.empty();
        }

        if (bytesRead == CONNECTION_ERROR) {
            throw new ConnectionClosedError("Connection closed");
        }

        return Optional.of(new String(Arrays.copyOfRange(buffer.array(), 0, bytesRead)));
    }

    private DataExchangeUtils(){
    }
}
