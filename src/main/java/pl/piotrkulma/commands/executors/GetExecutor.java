package pl.piotrkulma.commands.executors;

import pl.piotrkulma.commands.DataExchangeUtils;
import pl.piotrkulma.session.Session;

import java.nio.channels.SocketChannel;
import java.util.Optional;

public class GetExecutor implements Executor {
    @Override
    public void execute(
            final Optional<String> lastClientAction,
            final SocketChannel clientChannel,
            final Session session) {

        String fileName = lastClientAction.get().split(" ")[1];
        DataExchangeUtils.sendDataFromFile(clientChannel, fileName);
        session.addAction("SENDING TRANSFER to " + fileName);
    }
}
