package pl.piotrkulma.commands.executors;

import pl.piotrkulma.commands.DataExchangeUtils;
import pl.piotrkulma.session.Session;

import java.nio.channels.SocketChannel;
import java.util.Optional;

public class PostExecutor implements Executor {
    @Override
    public void execute(
            final Optional<String> lastClientAction,
            final SocketChannel clientChannel,
            final Session session) {

        String fileName = lastClientAction.get().split(" ")[1];
        DataExchangeUtils.saveDataToFile(clientChannel, fileName);
        session.addAction("RECEIVING TRANSFER to " + fileName);
    }
}
