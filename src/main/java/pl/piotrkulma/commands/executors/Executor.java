package pl.piotrkulma.commands.executors;

import pl.piotrkulma.session.Session;

import java.nio.channels.SocketChannel;
import java.util.Optional;

public interface Executor {
    void execute(final Optional<String> lastClientAction, final SocketChannel clientChannel, final Session session);
}
