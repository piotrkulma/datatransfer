package pl.piotrkulma.commands;

import pl.piotrkulma.commands.executors.Executor;
import pl.piotrkulma.commands.executors.GetExecutor;
import pl.piotrkulma.commands.executors.PostExecutor;
import pl.piotrkulma.server.SessionAction;
import pl.piotrkulma.session.Session;
import pl.piotrkulma.session.SessionHolder;

import java.nio.channels.SocketChannel;
import java.util.Optional;

public class CommandExecutor {
    private Executor postExecutor;
    private Executor getExecutor;

    public CommandExecutor() {
        this.postExecutor = new PostExecutor();
        this.getExecutor = new GetExecutor();
    }

    public void execute(
            final SessionHolder sessionHolder,
            final SocketChannel clientChannel,
            final Session session) {
        Optional<String> lastClientAction = CommandSessionUtil.lastSessionAction(sessionHolder, session.getId());
        SessionAction lastSessionActionType = CommandSessionUtil.lastSessionActionType(sessionHolder, session.getId());

        if (lastSessionActionType == SessionAction.COMMAND_POST) {
            postExecutor.execute(lastClientAction, clientChannel, session);
        } else if (lastSessionActionType == SessionAction.COMMAND_GET) {
            getExecutor.execute(lastClientAction, clientChannel, session);
        } else if (lastSessionActionType == SessionAction.COMMAND || lastSessionActionType == SessionAction.EMPTY) {
            executeOtherCommand(lastClientAction, clientChannel, session);
        }
    }

    //TODO other commands
    private void executeOtherCommand(
            final Optional<String> lastClientAction,
            final SocketChannel clientChannel,
            final Session session) {
        Optional<String> command = DataExchangeUtils.fetchStringData(clientChannel);
        if (command.isPresent()) {
            session.addAction(command.get());
        } else {
        }
    }
}
