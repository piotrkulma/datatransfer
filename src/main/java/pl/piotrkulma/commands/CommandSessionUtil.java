package pl.piotrkulma.commands;

import pl.piotrkulma.server.SessionAction;
import pl.piotrkulma.session.Session;
import pl.piotrkulma.session.SessionHolder;

import java.util.Optional;

public final class CommandSessionUtil {
    private static final String POST_COMMAND = "post";
    private static final String GET_COMMAND = "get";

    public static SessionAction lastSessionActionType(final SessionHolder sessionHolder, final String sessionId) {
        Optional<Session> clientOptional = sessionHolder.findSession(sessionId);
        Session session = clientOptional.get();

        Optional<String> lastCommandOptional = session.getLastAction();

        if (lastCommandOptional.isPresent()) {
            if (lastCommandOptional.get().contains(POST_COMMAND)) {
                return SessionAction.COMMAND_POST;
            } else if (lastCommandOptional.get().contains(GET_COMMAND)) {
                return SessionAction.COMMAND_GET;
            }

            return SessionAction.COMMAND;
        }

        return SessionAction.EMPTY;
    }

    public static Optional<String> lastSessionAction(final SessionHolder sessionHolder, final String sessionId) {
        Optional<Session> clientOptional = sessionHolder.findSession(sessionId);
        Session session = clientOptional.get();

        Optional<String> lastCommandOptional = session.getLastAction();

        if (lastCommandOptional.isPresent()) {
            return Optional.of(lastCommandOptional.get());
        }

        return Optional.empty();
    }

    private CommandSessionUtil() {

    }
}
