package pl.piotrkulma.server;

public enum SessionAction {
    EMPTY, COMMAND, COMMAND_GET, COMMAND_POST
}
