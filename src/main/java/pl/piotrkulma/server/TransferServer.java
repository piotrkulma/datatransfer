package pl.piotrkulma.server;

import pl.piotrkulma.commands.CommandExecutor;
import pl.piotrkulma.server.exceptions.ConnectionClosedError;
import pl.piotrkulma.session.Session;
import pl.piotrkulma.session.SessionHolder;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;

public class TransferServer {
    private int portNumber;
    private SessionHolder sessionHolder;
    private CommandExecutor commandExecutor;
    private ServerSocketChannel serverSocketChannel;

    public TransferServer(final int portNumber) {
        this.portNumber = portNumber;
        this.sessionHolder = new SessionHolder();
        this.commandExecutor = new CommandExecutor();
    }

    public void startServer() throws IOException {
        configureServerSocket();

        Selector selector = Selector.open();
        this.serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);

        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iter = selectedKeys.iterator();

            iterateSelectionKeys(iter, selector);
        }
    }

    private void configureServerSocket() {
        try {
            this.serverSocketChannel = ServerSocketChannel.open();
            this.serverSocketChannel.socket().bind(new InetSocketAddress(this.portNumber));
            this.serverSocketChannel.configureBlocking(false);
        } catch (IOException e) {
            throw new RuntimeException("Error while server socket configuration: " + e.getMessage());
        }
    }

    private void iterateSelectionKeys(final Iterator<SelectionKey> iter, final Selector selector) {
        while (iter.hasNext()) {
            SelectionKey key = iter.next();

            if (key.isAcceptable()) {
                ServerSessionUtil.registerSession(serverSocketChannel, sessionHolder, selector);
            }

            if (key.isReadable()) {
                SocketChannel clientChannel = (SocketChannel) key.channel();
                String sessionId = ServerSessionUtil.getSessionId(clientChannel);

                Optional<Session> clientOptional = sessionHolder.findSession(sessionId);
                Session session = clientOptional.get();

                try {
                    commandExecutor.execute(sessionHolder, clientChannel, session);
                } catch (ConnectionClosedError e) {
                    ServerSessionUtil.unregisterSession(clientChannel, sessionHolder);
                }
            }
        }
    }
}
