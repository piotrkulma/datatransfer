package pl.piotrkulma.server.exceptions;

public class DataTransferError extends RuntimeException {
    public DataTransferError(final String message) {
        super(message);
    }
}
