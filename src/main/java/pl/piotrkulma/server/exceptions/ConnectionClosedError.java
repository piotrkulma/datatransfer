package pl.piotrkulma.server.exceptions;

public class ConnectionClosedError extends RuntimeException {
    public ConnectionClosedError(final String message) {
        super(message);
    }
}
