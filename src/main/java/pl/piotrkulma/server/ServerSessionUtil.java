package pl.piotrkulma.server;

import pl.piotrkulma.session.SessionHolder;

import java.io.IOException;
import java.nio.channels.*;

public final class ServerSessionUtil {
    public static void registerSession(
            final ServerSocketChannel serverSocketChannel,
            final SessionHolder sessionHolder,
            final Selector selector) {

        try {
            SocketChannel clientSocket = serverSocketChannel.accept();
            if (clientSocket != null) {
                String sessionId = getSessionId(clientSocket);
                sessionHolder.createSession(sessionId);
                clientSocket.configureBlocking(false);
                clientSocket.register(selector, SelectionKey.OP_READ);

                System.out.println("New session " + sessionId);
            }
        } catch (IOException e) {
            throw new RuntimeException("Error while registering session: " + e.getMessage());
        }
    }

    public static void unregisterSession(final SocketChannel clientChannel, final SessionHolder sessionHolder) {

        try {
            String sessionId = getSessionId(clientChannel);

            System.out.println("Disconnected " + sessionId);
            sessionHolder.removeSession(sessionId);
            clientChannel.close();
        } catch (IOException e) {
            throw new RuntimeException("Error while unregistering session: " + e.getMessage());
        }
    }

    public static String getSessionId(final SocketChannel clientSocket) {
        try {
            return clientSocket.getRemoteAddress().toString();
        } catch (IOException e) {
            throw new RuntimeException("Error while fetching session id from SocketChannel: " + e.getMessage());
        }
    }

    private ServerSessionUtil() {
    }
}
