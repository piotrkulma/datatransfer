package pl.piotrkulma.server;

import java.io.IOException;

public class TransferServerMain {
    public static void main(final String[] args) throws IOException {
        TransferServer transferServer = new TransferServer(9999);
        transferServer.startServer();
    }
}
